package Q2;
/**
 * @author Louisa Yuxin Lin
 *
 */
public class SalariedEmployee implements Employee {
	private int annualSalary;
	public SalariedEmployee(int salary) {
		this.annualSalary=salary;
	}
	public int getYearlyPay() {
		return this.annualSalary;
}
}