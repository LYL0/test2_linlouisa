package Q3
import java.util.*;
//Louisa Yuxin Lin 1933472
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	/**
	*This is the overriding of equals()method. This should only return true if the two planets' names
	and orders are identical.
	*/
	public boolean equals(Planet p){
		
		if(this.getName == p.getName){
			if(this.getOrder == p.getOrder){
				return true;
			}
		}
		return false;
	}
	/**
	*This is the ovveriding of Hashcode() method. It is modified so Planet objects can be used within 
	various collections classes.
	*/
	@Override
     public int hashCode() {
		 String combine= this.name + this.order;
		 return combination.hashCode();	
}
	public int compareTo(Planet planetExample){
		int nameComparasion= this.getName().compareTo(planetExample.getName());
		int systemComparasion=this.getPlanetarySystemName().compareTo(planetExample.getPlanetarySystemName());
		if(nameComparasion == 0){
			return systemComparasion;
		}
		else{
			return nameComparasion;
		}
	}
}
