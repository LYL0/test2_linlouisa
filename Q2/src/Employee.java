package Q2;
/**
 * @author Louisa Yuxin Lin
 *
 */
public interface Employee {
	int getYearlyPay();

}
