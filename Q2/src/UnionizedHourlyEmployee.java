package Q2;
/**
 * 
 * @author Louisa Yuxin Lin
 *
 */
public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int hoursInWeek;
	private int hourlyPay;
	private int annualSalary;
	private int pensionContribution;
	public UnionizedHourlyEmployee(int hours,int hourlyPay, int pensionContribution) {
		super(hours,hourlyPay);
		this.pensionContribution=pensionContribution;
	}

	@Override
	public int getYearlyPay() {
		annualSalary=hoursInWeek*hourlyPay*52+pensionContribution;
		return annualSalary;
	}
	
	

}
