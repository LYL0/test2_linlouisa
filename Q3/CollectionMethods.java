package Q3
import java.util.*;
/**
*
*@author Louisa Yuxin Lin
*/
//This is Question 4, but it belongs in Q3 package because they are related.
public class ColletionMethods{
	public static Collection<Planet> getInnerPlanets(Collection<planet>planets){
		ArrayList<Planet> aftersorting = new ArrayList<Planet>();
		//For each loop to go through the input 
		//since no modification is required.
		for(Planet p:planets){
			if(aftersorting.size()<=3){
				//If this planet comes infront of the next, add it to the arrayList.
				if(p.getName().compareTo(p.getName())<0){
					aftersorting.add(p);
				}
			}
		}
		return aftersorting;
	}
}