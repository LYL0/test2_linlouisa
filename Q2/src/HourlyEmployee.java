package Q2;
/**
 * @author Louisa Yuxin Lin
 */
public class HourlyEmployee implements Employee {
	private int hoursInWeek;
	private int hourlyPay;
	private int annualSalary;
	public HourlyEmployee(int hours,int hourlyPay) {
		this.hoursInWeek=hours;
		this.hourlyPay=hourlyPay;
	}
	public int getYearlyPay() {
		/*
		 * 52 is the number of weeks in a year
		 */
		annualSalary=hoursInWeek*hourlyPay*52;
		return annualSalary;
	}

	

}
